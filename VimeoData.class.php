<?php

namespace ProcessWire;

class VimeoData extends WireData
{

  function __construct()
  {
    $this->clear();
  }

  public function clear()
  {
    $this->set('id', null);
    $this->set('name', null);
    $this->set('description', null);
    $this->set('link', null);
    $this->set('width', null);
    $this->set('height', null);
    $this->set('ratio', null);
    $this->set('duration', null);
    $this->set('pictures', null);
    $this->set('dates', null);
    $this->set('embed', null);
    $this->set('user_id', null);
    $this->set('user_link', null);
  }

  public function isValid()
  {
    return ($this->get('link') == '' ? false : true);
  }

  public function set($key, $value)
  {
    return parent::set($key, $value);
  }

  public function get($key)
  {
    if ($key == 'statusString') return $this->get('id');
    if ($key == 'ratio' && !parent::get($key)) {
      return $this->calculateRatio();
    }
    return parent::get($key);
  }

  public function calculateRatio()
  {
    if (parent::get('width') && parent::get('height')) {
      $gcd = $this->GCD(intval(parent::get('width')), intval(parent::get('height')));
      return intval(parent::get('width')) / $gcd . ':' . intval(parent::get('height')) / $gcd;
    }
    return '';
  }

  public function __toString()
  {
    if (!$this->isValid()) {
      return 'No valid vimeo link selected';
    }
    return $this->get('link') . ' —-- ' . $this->get('name');
  }

  private function GCD($a, $b)
  {
    while ($b != 0) {
      $remainder = $a % $b;
      $a = $b;
      $b = $remainder;
    }
    return abs($a);
  }
}
